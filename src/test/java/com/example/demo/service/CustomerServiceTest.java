package com.example.demo.service;

import com.example.demo.config.User;
import com.example.demo.dto.CustomerDTO;
import com.example.demo.entity.Customer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.ReactiveSecurityContextHolder;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ExtendWith(SpringExtension.class)
class CustomerServiceTest {

    @Autowired
    private CustomerService customerService;

    @Test
    void getCustomer() {
        Mono<Customer> customer = customerService.getCustomer(1l);
        StepVerifier
                .create(customer)
                .expectNextCount(1)
                .verifyComplete();

    }

    @Test
    void addCustomer() {
        CustomerDTO customer = CustomerDTO.builder()
                .id(null)
                .name("Test")
                .age(20)
                .build();

        StepVerifier.create(customerService.addCustomer(customer))
                .expectNextCount(1)
                .verifyComplete();
    }

    @BeforeEach
    void setUp() {
        User principal = new User();
        principal.setId(1l);
        var auth = new UsernamePasswordAuthenticationToken(principal, "test");
        ReactiveSecurityContextHolder.withAuthentication(auth);
    }
}