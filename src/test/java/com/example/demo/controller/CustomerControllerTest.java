package com.example.demo.controller;

import com.example.demo.config.JWTUtil;
import com.example.demo.config.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.reactive.server.WebTestClient;

import java.util.Arrays;

import static com.example.demo.dto.Role.ROLE_ADMIN;
import static com.example.demo.dto.Role.ROLE_USER;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class CustomerControllerTest {

    @Autowired
    private WebTestClient webTestClient;

    @Autowired
    JWTUtil jwtUtil;

    @Test
    void hello() {
        var user = new User();
        user.setId(1l);
        user.setUsername("admin");
        user.setPassword("123456");
        user.setRoles(Arrays.asList(ROLE_USER, ROLE_ADMIN));
        var token = jwtUtil.generateToken(user);
        var name = "Thanh";
        webTestClient
                .get().uri("/v2/hello?name={name}", name)
                .accept(MediaType.APPLICATION_JSON)
                .header("Authorization", "Bearer " + token)
                .exchange()
                .expectStatus().isOk()
                .expectBody(String.class).isEqualTo("Hello " + name);
    }
}