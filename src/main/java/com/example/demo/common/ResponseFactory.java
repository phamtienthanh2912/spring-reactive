package com.example.demo.common;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

@Component
public class ResponseFactory {

    public Mono<ResponseEntity<Response>> success(Object data) {
        return Mono.just(ResponseEntity.ok(new Response("Success", "", data)));
    }

    public ResponseEntity<Response> error(String message, HttpStatus httpStatus) {
        return ResponseEntity.status(httpStatus).body(new Response("Error", message, null));
    }

    public ResponseEntity<Response> successV2(Object data) {
        return ResponseEntity.ok(new Response("Success", "", data));
    }
}
