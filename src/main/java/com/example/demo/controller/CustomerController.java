package com.example.demo.controller;

import com.example.demo.common.Response;
import com.example.demo.common.ResponseFactory;
import com.example.demo.dto.CustomerDTO;
import com.example.demo.dto.request.SearchCustomer;
import com.example.demo.service.CustomerService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Slf4j
@RestController
@AllArgsConstructor
@RequestMapping("/v2")
public class CustomerController {
    CustomerService customerService;
    final ResponseFactory responseFactory;

    @GetMapping("/hello")
    public Mono<String> hello(@RequestParam String name) {
        log.debug("CustomerController hello: {}", name);
        return Mono.just("Hello " + name);
    }

    @GetMapping("/customer")
    public Flux<CustomerDTO> getAllCustomer() {
        return customerService.getAllCustomer();
    }

    @GetMapping("/customer/{id}")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public Mono<ResponseEntity<Response>> getCustomer(@PathVariable Long id) {
        return customerService.getCustomer(id).flatMap(responseFactory::success);
    }

    @PostMapping("/customer/search")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public Flux<CustomerDTO> search(@RequestBody SearchCustomer searchCustomer) {
        return customerService.searchCustomer(searchCustomer);
    }

    @GetMapping("/customer/all")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public Flux<CustomerDTO> allCustomer() {
        return customerService.getAll();
    }

    @PostMapping("/customer")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public Mono<ResponseEntity<Response>> add(@RequestBody CustomerDTO customerDTO) {
        return customerService.addCustomer(customerDTO).flatMap(o -> responseFactory.success(true));
    }

    @DeleteMapping("/customer/{id}")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public Mono<ResponseEntity<Response>> delete(@PathVariable Long id) {
        return customerService.delete(id).flatMap(responseFactory::success);
    }

    @PutMapping("/customer")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public Mono<ResponseEntity<Response>> edit(@RequestBody CustomerDTO customerDTO) {
        return customerService.update(customerDTO).flatMap(o -> responseFactory.success(true));
    }
}
