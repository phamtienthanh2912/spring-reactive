package com.example.demo.controller;

import com.example.demo.common.Response;
import com.example.demo.common.ResponseFactory;
import com.example.demo.config.JWTUtil;
import com.example.demo.config.User;
import com.example.demo.dto.AuthRequest;
import com.example.demo.dto.AuthResponse;
import com.example.demo.service.UserDetailsService;
import com.example.demo.utils.SecurityUtil;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.ReactiveAuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.ReactiveSecurityContextHolder;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@AllArgsConstructor
@RestController
@Slf4j
public class AuthController {
    final JWTUtil jwtUtil;
    final UserDetailsService userDetailsService;

    final ReactiveAuthenticationManager reactiveAuthenticationManager;
    final ResponseFactory responseFactory;

    @PostMapping("/login")
    public Mono<ResponseEntity<AuthResponse>> login(@RequestBody Mono<AuthRequest> authRequest) {
        return authRequest.flatMap(ar -> reactiveAuthenticationManager.authenticate(new UsernamePasswordAuthenticationToken(ar.getUsername(), ar.getPassword()))).map(auth -> {
            ReactiveSecurityContextHolder.getContext().contextWrite(ReactiveSecurityContextHolder.withAuthentication(auth));
            User user = (User) auth.getPrincipal();
            return ResponseEntity.ok(new AuthResponse(jwtUtil.generateToken(user)));
        }).switchIfEmpty(Mono.just(ResponseEntity.status(HttpStatus.UNAUTHORIZED).build()));
    }

    @PostMapping("/register")
    public Mono<ResponseEntity<Response>> register(@RequestBody AuthRequest authRequest) {
        return userDetailsService.register(authRequest).flatMap(account -> responseFactory.success("True"));
    }

    @PostMapping("/me")
    public Mono<ResponseEntity<Response>> me() {
        return ReactiveSecurityContextHolder.getContext().map(SecurityContext::getAuthentication).flatMap(responseFactory::success);
    }

    @PostMapping("/Id")
    public Mono<ResponseEntity<Response>> id() {
        Mono<Long> longMono = SecurityUtil.currentUserId();
        return longMono.flatMap(responseFactory::success);
    }


}
