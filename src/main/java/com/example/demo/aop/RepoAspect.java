package com.example.demo.aop;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;

@Aspect
@Configuration
public class RepoAspect {
    private static Logger log = LoggerFactory.getLogger(RepoAspect.class);
    private int executionLimitMs = 1000;

    @Around("execution(* com.example.demo.repository.*.*.*(..))")
    public Object logExecutionTime(ProceedingJoinPoint joinPoint) throws Throwable {
        long start = System.currentTimeMillis();
        Object proceed = joinPoint.proceed();
        long executionTime = System.currentTimeMillis() - start;
        String message = joinPoint.getSignature() + " exec in " + executionTime + " ms";
        if (executionTime >= executionLimitMs) {
            log.debug("======================================");
            log.debug("{} {}", message, " : SLOW QUERY");
            log.debug("======================================");

        }
        return proceed;
    }

//    @Around("execution(* com.example.demo.service.CustomerV2Service.getAll(..))")
//    public Object aroundService(ProceedingJoinPoint joinPoint) throws Throwable {
//        //Object proceed = joinPoint.proceed();
//        return Flux.just(new CustomerDTO());
//    }
}
