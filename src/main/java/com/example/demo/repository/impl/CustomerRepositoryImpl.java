package com.example.demo.repository.impl;

import com.example.demo.dto.CustomerDTO;
import com.example.demo.entity.Customer;
import com.example.demo.repository.CustomerRepositoryCustom;
import io.r2dbc.spi.Row;
import io.r2dbc.spi.RowMetadata;
import lombok.RequiredArgsConstructor;
import org.springframework.data.r2dbc.core.R2dbcEntityTemplate;
import org.springframework.data.relational.core.query.Query;
import org.springframework.data.relational.core.query.Update;
import org.springframework.r2dbc.core.DatabaseClient;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.function.BiFunction;

import static org.springframework.data.relational.core.query.Criteria.where;

@Repository
@RequiredArgsConstructor
public class CustomerRepositoryImpl implements CustomerRepositoryCustom {

    final DatabaseClient databaseClient;
    final R2dbcEntityTemplate r2dbcEntityTemplate;

    public static final BiFunction<Row, RowMetadata, Customer> biFunction = (row, rowMetaData) -> Customer.builder()
            .id(row.get("id", Long.class))
            .name(row.get("name", String.class))
            .age(row.get("age", Integer.class))
            .build();

    public Flux<Customer> getCustomer() {
        return databaseClient.sql("select * from customer")
                .filter((statement, executeFunction) -> statement.fetchSize(10).execute())
                .map(biFunction).all();
    }

    public Mono<Integer> update(CustomerDTO customerDTO) {
        return r2dbcEntityTemplate.update(
                Query.query(where("id").is(customerDTO.getId())),
                Update.update("name", customerDTO.getName()).set("age", customerDTO.getAge()),
                Customer.class);
    }
}
