package com.example.demo.repository;

import com.example.demo.entity.User;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

@Repository
public interface UserRepository extends ReactiveCrudRepository<User, Long> {
    Mono<User> findByUsernameAndStatus(String username, int status);

    Mono<User> findByUsername(String username);

}
