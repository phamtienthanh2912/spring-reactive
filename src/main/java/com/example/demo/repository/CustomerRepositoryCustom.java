package com.example.demo.repository;

import com.example.demo.entity.Customer;
import reactor.core.publisher.Flux;

public interface CustomerRepositoryCustom {

    Flux<Customer> getCustomer();
}
