package com.example.demo;

import reactor.core.publisher.Mono;

import java.io.IOException;

public class Test {
    public static void main(String[] args) throws IOException {
        Mono<String> mono = Mono.just("c");
        //Mono<String> mono = Mono.empty();
        //Mono<Object> mono = Mono.error(() -> new BusinessException("a"));

        //mono.subscribe(s -> System.out.println(s));
        //mono.then(Mono.just("b")).subscribe(s -> System.out.println(s));
        //String a = mono.block();
        //System.out.println(a);


        //mono.doOnSuccess(s -> System.out.println(s));
        //mono.doOnNext(s -> System.out.println(s));
        mono.log().thenReturn(1).subscribe(System.out::println);
        System.in.read();
    }
}
