package com.example.demo.service;

import com.example.demo.dto.CustomerDTO;
import com.example.demo.dto.request.SearchCustomer;
import com.example.demo.entity.Customer;
import com.example.demo.exception.BusinessException;
import com.example.demo.exception.NotFoundException;
import com.example.demo.mapper.CustomerMapper;
import com.example.demo.repository.CustomerRepository;
import com.example.demo.utils.SecurityUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Instant;
import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor
public class CustomerService {
    final CustomerRepository customerRepository;
    final CustomerMapper customerMapper;
    final WebClient webClient;

    public Flux<CustomerDTO> getAll() {
        return customerRepository.findAll().flatMap(customer -> Mono.just(customerMapper.toDto(customer)));
    }

    @Transactional(readOnly = true)
    public Flux<CustomerDTO> getAllCustomer() {
        return customerRepository.getCustomer().map(customerMapper::toDto);
    }

    public Mono<Customer> getCustomer(Long id) {
        return customerRepository.findById(id).switchIfEmpty(Mono.error(new NotFoundException("No Customer was found with id: " + id)));
    }

    public Flux<CustomerDTO> searchCustomer(SearchCustomer searchCustomer) {
        return customerRepository.findByNameLike(searchCustomer.getName()).flatMap(customer -> Mono.just(customerMapper.toDto(customer)));
    }

    public Mono<Customer> addCustomer(CustomerDTO customerDTO) {
        var customer = new Customer();
        customer.setName(customerDTO.getName());
        customer.setCreatedAt(Instant.now());
        var userIdMono = SecurityUtil.currentUserId().switchIfEmpty(Mono.error(new NotFoundException("User login not found")));
        var customerMono = userIdMono.flatMap(userId -> {
            customer.setCreatedBy(userId);
            return Mono.just(customer);
        });

        //        Mono<CustomerDTO> objectMono = webClient.post()
        //                .uri("/google")
        //                .acceptCharset(StandardCharsets.UTF_8)
        //                .body(Mono.just(customerDTO), CustomerDTO.class)
        //                .retrieve()
        //                .bodyToMono(CustomerDTO.class);

        return customerMono.flatMap(customerRepository::save).switchIfEmpty(Mono.error(new BusinessException("Create customer error")));
    }

    public Mono<Boolean> delete(Long id) {
        return customerRepository.deleteById(id).then(Mono.just(true));
    }

    public Mono<Customer> update(CustomerDTO customerDTO) {
        var id = Optional.ofNullable(customerDTO.getId()).orElseThrow(() -> new BusinessException("Id must not null"));
        var customerMono = customerRepository.findById(id)
                .switchIfEmpty(Mono.error(new BusinessException("Customer not found with id: " + id)))
                .zipWith(SecurityUtil.currentUserId().switchIfEmpty(Mono.error(new NotFoundException("User login not found"))))
                .map(tuple -> {
                    Customer customer = tuple.getT1();
                    Long userId = tuple.getT2();
                    customer.setName(customerDTO.getName());
                    customer.setLastModifiedDate(Instant.now());
                    customer.setAge(customerDTO.getAge());
                    customer.setLastModifiedBy(userId);
                    return customer;
                });

        return customerMono.flatMap(customerRepository::save).switchIfEmpty(Mono.error(new BusinessException("Edit customer error")));
    }
}
