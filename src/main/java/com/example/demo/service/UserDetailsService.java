package com.example.demo.service;

import com.example.demo.config.User;
import com.example.demo.dto.AuthRequest;
import com.example.demo.dto.Role;
import com.example.demo.exception.BusinessException;
import com.example.demo.exception.NotFoundException;
import com.example.demo.repository.UserRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Primary;
import org.springframework.security.core.userdetails.ReactiveUserDetailsService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.util.Arrays;

@Service
@AllArgsConstructor
@Primary
@Slf4j
public class UserDetailsService implements ReactiveUserDetailsService {
    final UserRepository userRepository;
    final PasswordEncoder passwordEncoder;

    @Override
    public Mono<UserDetails> findByUsername(String username) {
        return userRepository.findByUsernameAndStatus(username, 1).switchIfEmpty(Mono.error(new NotFoundException("Username or password incorrect"))).flatMap(user -> Mono.just(new User(user.getUsername(), user.getPassword(), user.getStatus() == 1, Arrays.asList(Role.ROLE_USER, Role.ROLE_ADMIN), user.getId())));
    }

    public Mono<Object> register(AuthRequest authRequest) {
        final var userByUsername = userRepository.findByUsername(authRequest.getUsername());
        log.info("check username in db: {}", authRequest.getUsername());
        return userByUsername.flatMap(tmp -> Mono.error(new BusinessException("User exists in database"))).switchIfEmpty(createAccount(authRequest));
    }

    private Mono<com.example.demo.entity.User> createAccount(AuthRequest authRequest) {
        var user = new com.example.demo.entity.User();
        user.setUsername(authRequest.getUsername());
        user.setPassword(passwordEncoder.encode(authRequest.getPassword()));
        user.setStatus(1);
        user.setFullName(authRequest.getFullName());
        return userRepository.save(user);
    }
}
