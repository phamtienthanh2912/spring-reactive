package com.example.demo.exception;

import com.example.demo.common.ResponseFactory;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
@RequiredArgsConstructor
@Slf4j
public class ApplicationExceptionHandler {

    final ResponseFactory responseFactory;

    @ExceptionHandler({NotFoundException.class, BusinessException.class})
    public ResponseEntity<?> handlerCustomerNotFoundException(Exception e) {
        log.info(e.getMessage(), e);
        return responseFactory.error(e.getMessage(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({Exception.class})
    public ResponseEntity<?> handlerException(Exception e) {
        log.info(e.getMessage(), e);
        return responseFactory.error(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
