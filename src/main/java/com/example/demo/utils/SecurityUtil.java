package com.example.demo.utils;

import com.example.demo.config.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.ReactiveSecurityContextHolder;
import reactor.core.publisher.Mono;

@Slf4j
public class SecurityUtil {

    public static Mono<Long> currentUserId() {
        return ReactiveSecurityContextHolder.getContext().map(securityContext -> securityContext.getAuthentication()).flatMap(authentication -> {
            if (authentication instanceof UsernamePasswordAuthenticationToken) {
                User principal = (User) authentication.getPrincipal();
                return Mono.just(principal.getId());
            }
            return Mono.empty();
        });
    }
}
