package com.example.demo.entity;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

import java.time.Instant;

@Setter
@Getter
@Table(name = "customer")
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Customer {
    @Id
    private Long id;
    @Column("name")
    private String name;
    @Column("age")
    private Integer age;
    @Column("status")
    private Integer status;
    @Column("created_by")
    private Long createdBy;
    @Column("created_at")
    private Instant createdAt;
    @Column("last_modified_by")
    private Long lastModifiedBy;
    @Column("last_modified_date")
    private Instant lastModifiedDate;

}
