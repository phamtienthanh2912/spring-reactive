package com.example.demo.config;

import com.example.demo.dto.Role;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.security.Key;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Component
public class JWTUtil {
    Logger log = LoggerFactory.getLogger(JWTUtil.class);
    @Value("${springbootwebfluxjjwt.jjwt.secret}")
    private String secret;

    @Value("${springbootwebfluxjjwt.jjwt.expiration}")
    private String expirationTime;

    private Key key;

    @PostConstruct
    public void init() {
        this.key = Keys.hmacShaKeyFor(secret.getBytes());
    }

    public Claims getAllClaimsFromToken(String token) {
        return Jwts.parserBuilder().setSigningKey(key).build().parseClaimsJws(token).getBody();
    }

    public String getUsernameFromToken(String token) {
        return getAllClaimsFromToken(token).getSubject();
    }

    public Date getExpirationDateFromToken(String token) {
        return getAllClaimsFromToken(token).getExpiration();
    }

    private Boolean isTokenExpired(String token) {
        final var expiration = getExpirationDateFromToken(token);
        return expiration.before(new Date());
    }

    public String generateToken(User user) {
        Map<String, Object> claims = new HashMap<>();
        claims.put("role", user.getRoles().stream().map(Enum::name).collect(Collectors.joining(",")));
        claims.put("id", user.getId());
        return doGenerateToken(claims, user.getUsername());
    }

    private String doGenerateToken(Map<String, Object> claims, String username) {
        var expirationTimeLong = Long.parseLong(expirationTime); //in second
        final var createdDate = new Date();
        final var expirationDate = new Date(createdDate.getTime() + expirationTimeLong * 1000);
        return Jwts.builder().setClaims(claims).setSubject(username).setIssuedAt(createdDate).setExpiration(expirationDate).signWith(key).compact();
    }

    public Authentication getAuthentication(String token) {
        try {
            var claims = getAllClaimsFromToken(token);
            var roles = Pattern.compile(",").splitAsStream(claims.get("role").toString()).map(Role::valueOf).toList();
            var userId = Long.valueOf(claims.get("id").toString());
            var principal = new User(claims.getSubject(), "", true, roles, userId);
            return new UsernamePasswordAuthenticationToken(principal, token, principal.getAuthorities());
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    public Boolean validateToken(String token) {
        return !isTokenExpired(token);
    }
}
