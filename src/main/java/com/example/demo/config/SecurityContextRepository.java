package com.example.demo.config;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.ReactiveAuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextImpl;
import org.springframework.security.web.server.context.ServerSecurityContextRepository;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

@AllArgsConstructor
public class SecurityContextRepository implements ServerSecurityContextRepository {
    private ReactiveAuthenticationManager reactiveAuthenticationManager;

    @Override
    public Mono<Void> save(ServerWebExchange exchange, SecurityContext context) {
        return null;
    }

    @Override
    public Mono<SecurityContext> load(ServerWebExchange swe) {
        return Mono.justOrEmpty(swe.getRequest()
                                   .getHeaders()
                                   .getFirst(HttpHeaders.AUTHORIZATION))
                   .filter(authHeader -> authHeader.startsWith("Bearer "))
                   .flatMap(authHeader -> {
                       String authToken = authHeader.substring(7);
                       Authentication auth = new UsernamePasswordAuthenticationToken(authToken, authToken);
                       return reactiveAuthenticationManager.authenticate(auth)
                                                        .map(SecurityContextImpl::new);
                   });
    }
}
