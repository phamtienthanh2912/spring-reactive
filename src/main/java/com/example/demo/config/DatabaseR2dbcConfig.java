package com.example.demo.config;

import io.r2dbc.spi.ConnectionFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.r2dbc.repository.config.EnableR2dbcRepositories;
import org.springframework.r2dbc.connection.R2dbcTransactionManager;
import org.springframework.transaction.ReactiveTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.reactive.TransactionalOperator;

@Configuration
@EnableR2dbcRepositories(basePackages = {"com.example.demo.repository"})
//https://hantsy.github.io/spring-reactive-sample/data/data-r2dbc.html
//https://medium.com/swlh/working-with-relational-database-using-r2dbc-databaseclient-d61a60ebc67f
//https://dzone.com/articles/r2dbc-reactive-programming-with-spring-part-4
@EnableTransactionManagement
public class DatabaseR2dbcConfig {

    //    @Bean
    //    public ConnectionFactory connectionFactory() {
    //        return ConnectionFactories.get(
    //                ConnectionFactoryOptions.builder()
    //                        .option(DRIVER, "postgresql")
    //                        .option(HOST, "localhost")
    //                        .option(USER, "user")
    //                        .option(PASSWORD, "secret")
    //                        .option(DATABASE, "studentdb")
    //                        .build());
    //    }

    @Bean
    ReactiveTransactionManager transactionManager(ConnectionFactory connectionFactory) {
        return new R2dbcTransactionManager(connectionFactory);
    }

    @Bean
    TransactionalOperator transactionalOperator(ReactiveTransactionManager transactionManager) {
        return TransactionalOperator.create(transactionManager);
    }
}

