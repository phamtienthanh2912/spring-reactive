package com.example.demo.dto;

public enum Role {
    ROLE_USER, ROLE_ADMIN
}
