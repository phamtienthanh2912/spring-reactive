FROM openjdk:17-alpine
ADD target/spring-reactive.jar spring-reactive.jar
EXPOSE 8080
WORKDIR /otp/root/docker/spring-reactive
ENTRYPOINT ["java","-jar","spring-reactive.jar"]